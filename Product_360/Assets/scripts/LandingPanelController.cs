﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Class controlling the behavior of Landing Panel
 * Animation is controlled from the animator (not from the script)
 */
public class LandingPanelController : MonoBehaviour 
{
	public Button guestButton;
	public Button loginSignupButton;
	private SceneController sceneController;

	void Start () 
	{
		//adding listener to "Guest" button
		guestButton.onClick.AddListener (OnGuestButtonClick);
		loginSignupButton.onClick.AddListener (OnLoginSignupButtonClicked);
		sceneController = GameObject.FindWithTag ("scene_controller").GetComponent<SceneController>();
	}
	//function to destroy Landing Panel and transition to 'open/new' panel
	void OnGuestButtonClick ()
	{
		sceneController.GoToNewOpenPanel ();
	}

	void OnLoginSignupButtonClicked ()
	{
		sceneController.GoToLoginSignupPanel ();
	}
}
