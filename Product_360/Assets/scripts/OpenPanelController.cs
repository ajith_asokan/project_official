﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Class controlling the behavior of  'Open/New' panel
 */
public class OpenPanelController : MonoBehaviour 
{
	public Button newButton;
	private SceneController sceneController;

	void Start () 
	{
		//adding listener
		newButton.onClick.AddListener (OnNewButtonClick);
		sceneController = GameObject.FindWithTag ("scene_controller").GetComponent<SceneController>();
	}

	//function tp destroy
	void OnNewButtonClick ()
	{
		sceneController.GoToViewer ();
	}
}
