﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

/*
 * Class controlling the overall application
 * All panels are created and destroyed from this class
 * All products (seen in viewer) undergo Instantiation in destruction in this class
 * Application starts and ends here
 */
public class SceneController : MonoBehaviour 
{
	//UI
	public Canvas canvas;
	public GameObject landingPanel;
	public GameObject openPanel;
	public GameObject viewerPanel;
	public GameObject colorPanel;
	public GameObject loginSignupPanel;

	//instances of UI for Destruction/memory release
	private GameObject landingPanelInstance;
	private GameObject openPanelInstance;
	private GameObject viewerPanelInstance;
	private GameObject colorPanelInstance;
	private GameObject loginSignupPanelInstance;

	//instance of cube for cleaning up
	private GameObject cubeInstance;

	//to access products currently open
	private string currentlyOpenObj;
	public GameObject objectInViewport;

	//for mouse behavior in viewer
	private Vector3 lastMousePosition;
	public float mouseSensitivity;
	private float temps;

	void Start () {
		//the landing panel is called when the app opens
		landingPanelInstance = Instantiate (landingPanel, canvas.transform);
	}
	
	// Update is called once per frame
	void Update () {
		Rect viewBound = new Rect (Screen.width-550.0f, 0, Screen.width/2, Screen.height);
		if ( Input.GetMouseButtonDown (0) )
		{
			temps = Time.time ;
		}

		//short click: for color panel instantiation
		if ( Input.GetMouseButtonUp (0) && (Time.time - temps) < 0.2)
		{
			//raycast to check if product is selected, so color-picker can be activated
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 200.0f)) 
			{
				Debug.Log ("You've selected " + hit.transform.name);
				//on clicking the product, the color picker is activated
				if(colorPanelInstance == null)
					colorPanelInstance = Instantiate (colorPanel, canvas.transform);
			}
		}

		//long click: for 360 viewing
		if ( Input.GetMouseButton (0) && (Time.time - temps) > 0.2 && viewBound.Contains(Input.mousePosition))
		{
			GameObject clickedObj = GameObject.FindGameObjectWithTag (currentlyOpenObj);
			//stop existing animation on the product to enable viewing
			Animator objAnim = clickedObj.GetComponent<Animator> ();
			Destroy (objAnim);
			//raycast for selection of product and viewing
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 200.0f)) 
			{
				Debug.Log ("You've selected " + hit.transform.name);
			}
			//mouse hold and drag is synchronized with product movement
			Vector3 rotateObj = new Vector3 (Input.GetAxis ("Mouse Y"), -(Input.GetAxis ("Mouse X")), 0);
			clickedObj.transform.Rotate (rotateObj*mouseSensitivity, Space.World);
		}
	}

	//destroying landing panel and moving on to "open/new" panel.
	public void GoToNewOpenPanel()
	{
		if (loginSignupPanelInstance != null) {
			Destroy (loginSignupPanelInstance);
			Debug.Log ("DestroyLoginSignupPanel");
		}
		if (landingPanelInstance != null) {
			Destroy (landingPanelInstance);
			Debug.Log ("DestroyLandingPanel");
		}
		openPanelInstance = Instantiate (openPanel, canvas.transform);
	}

	//destroying the "open/new" panel and instantiates the "Viewer" panel
	public void GoToViewer()
	{
		Destroy (openPanelInstance);
		Debug.Log ("DestroyOpenPanel");
		viewerPanelInstance = Instantiate (viewerPanel, canvas.transform);
		cubeInstance = Instantiate (objectInViewport);
		currentlyOpenObj = cubeInstance.tag;
	}

	//Open Good Ship
	public void OpenGood()
	{
		GameObject evilShip = GameObject.FindGameObjectWithTag ("model2");
		GameObject goodShip = GameObject.FindGameObjectWithTag ("model3");

		//Destroy other products (if activated) before instantiating "Good Ship"
		if (evilShip != null) 
			Destroy (evilShip);
		if (cubeInstance != null)
			Destroy (cubeInstance);
		if (goodShip == null) {
			Instantiate (Resources.Load ("Player Ship"));
			currentlyOpenObj = "model3";
		}
	}

	//Open Evil Ship
	public void OpenEvil()
	{
		GameObject evilShip = GameObject.FindGameObjectWithTag ("model2");
		GameObject goodShip = GameObject.FindGameObjectWithTag ("model3");
		//destroy other products if activated
		if (goodShip != null) 
			Destroy (goodShip);
		if (cubeInstance != null) 
			Destroy (cubeInstance);
		
		//instantiate Evil Ship from resources
		if (evilShip == null) {
			Instantiate (Resources.Load ("Enemy Ship"));
			currentlyOpenObj = "model2";
		}
	}

	//Applies the color, chosen from the color picker, on the product in viewport
	public void ApplyColor (Color iColor)
	{
		Renderer rend = GameObject.FindGameObjectWithTag (currentlyOpenObj).GetComponent<Renderer>();
		rend.material.color = iColor;

		//destroy color picker after choosing the color
		Destroy (colorPanelInstance,2.0f);
	}

	public void QuitApplication()
	{
		Destroy (viewerPanelInstance);
		//quit application based on platform
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#elif UNITY_WEBPLAYER
		Application.OpenURL(webplayerQuitURL);
		#else
		Application.Quit();
		#endif
	}

	public void GoToLoginSignupPanel()
	{
		Destroy (landingPanelInstance);
		Debug.Log ("DestroyLandingPanel");
		loginSignupPanelInstance = Instantiate (loginSignupPanel, canvas.transform);
	}

}
