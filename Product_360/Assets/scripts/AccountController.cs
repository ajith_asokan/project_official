﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;

/*
 * Class controlling the behavior of Login/Sign up panel
 * The credentials are stored in "Resources/Registry.txt" on Signup
 */
public class AccountController : MonoBehaviour {
	
	//fields and buttons to control login/signup
	public InputField usernameField;
	public InputField passwordField;
	public Button loginButton;
	public InputField newUsernameField;
	public InputField newPasswordField;
	public InputField retypePasswordField;
	public Button signupButton;

	//to read and write into Registry.txt
	private StreamWriter writer;
	private StreamReader reader;

	//username and password string for reading and writing into the Registry
	private string username;
	private string password;
	private string registryPath;

	//scene controller handle to access transition to the next panel and destroy current panel
	private SceneController sceneController;


	void Start () {
		registryPath = "Assets/Resources/Registry.txt";
		signupButton.onClick.AddListener (SignupClicked);
		loginButton.onClick.AddListener (LoginClicked);
		sceneController = GameObject.FindWithTag ("scene_controller").GetComponent<SceneController>();
	}

	void SignupClicked()
	{
		//writing new username and password into Registry.txt
		writer = new StreamWriter (registryPath, true);
		if (writer != null) {
			string form = "\n" + username + " " + password;
			writer.WriteLine (form);
			Debug.Log (form);
			if (form == "")
				Debug.Log ("Form is Empty!");
			writer.Close ();
			form = "";
			username = "";
			password = "";
			newUsernameField.text = "";
			newPasswordField.text = "";
			retypePasswordField.text = "";
			signupButton.interactable = false;
		}
		else 
		{
			Debug.LogWarning ("Registry.txt (writer) not found!");
		}
	}

	void LoginClicked()
	{
		//check username and password combination in Registry.txt
		reader = new StreamReader (registryPath, true);
		if (reader != null) {
			string form = username + " " + password;
			Debug.Log (form);
			string totalText = reader.ReadToEnd();
			if (totalText.Contains (form)) {
				Debug.Log ("successfully logged");
				loginButton.interactable = false;
				sceneController.GoToNewOpenPanel ();
			} else {
				loginButton.interactable = false;
				Debug.Log ("Invalid Credentials");
			}
		} 
		else 
		{
			Debug.Log ("Registry.txt (reader) not found");
		}
	}

	//activates login fields when signup fields are empty
	void ActivateLoginFields()
	{
		if (newUsernameField.text.Length == 0 && newPasswordField.text.Length == 0 && retypePasswordField.text.Length == 0) {
			usernameField.interactable = true;
			passwordField.interactable = true;
			if (usernameField.text.Length > 0 && passwordField.text.Length > 0) {
				username = usernameField.text;
				password = passwordField.text;
				loginButton.interactable = true;
			}
		} 
		else{
			usernameField.interactable = false;
			passwordField.interactable = false;
			loginButton.interactable = false;
		}
	}

	//activates signup fields when login fields are empty
	void ActivateSignupFields()
	{
		if (usernameField.text.Length == 0 && passwordField.text.Length== 0) {
			newUsernameField.interactable = true;
			newPasswordField.interactable = true;
			retypePasswordField.interactable = true;
			if (newUsernameField.text.Length > 0 && newPasswordField.text.Length > 0 && newPasswordField.text == retypePasswordField.text) {
				username = newUsernameField.text;
				password = newPasswordField.text;
				signupButton.interactable = true;
			}
		} 
		else{
			newUsernameField.interactable = false;
			newPasswordField.interactable = false;
			retypePasswordField.interactable = false;
			signupButton.interactable = false;
		}
	}

	void Update () {
		ActivateLoginFields ();
		ActivateSignupFields ();
	}
}
