﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Class controlling the behavior of viewer panel
 */
public class ViewerPanelController : MonoBehaviour
{
	public Button goodButton;
	public Button evilButton;
	public Button quitButton;
	private SceneController sceneController;
	public InputField productDescription;

	void Start () {
		sceneController = GameObject.FindWithTag ("scene_controller").GetComponent<SceneController>();

		//adding subscribers for events
		goodButton.onClick.AddListener (OnGoodButtonPressed);
		evilButton.onClick.AddListener (OnEvilButtonPressed);
		quitButton.onClick.AddListener (OnQuitButtonPressed);
	}

	//when "Good Ship" button is pressed
	void OnGoodButtonPressed()
	{
		//instantiate Good Ship from scene controller and update description
		sceneController.OpenGood();
		productDescription.text = "Ship Name: Millennium Falcon \nOwner: Hans Solo \nFaction: Rebel";
	}

	//when "Evil Ship" button is pressed
	void OnEvilButtonPressed()
	{
		//instantiate Evil Ship from scene controller and update description
		sceneController.OpenEvil();
		productDescription.text = "Ship Name: Star Destroyer \nOwner: Darth Vader \nFaction: Republic";
	}
	void OnQuitButtonPressed()
	{
		sceneController.QuitApplication ();
	}

}
