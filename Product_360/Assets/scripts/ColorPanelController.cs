﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//namespace for finding the current object selected
using UnityEngine.EventSystems;

/*
 * class controlling the behavior of Color Picker panel
 */ 
public class ColorPanelController : MonoBehaviour 
{
	public Button whiteButton;
	public Button redButton;
	public Button yellowButton;
	public Button greenButton;
	public Button blueButton;
	public Button blackButton;
	private Color chosenButtonColor;
	private SceneController sceneController;

	void Start () 
	{
		sceneController = GameObject.FindWithTag ("scene_controller").GetComponent<SceneController>();

		//adding listener to all button-click events
		whiteButton.onClick.AddListener (OnColorButtonClick);
		redButton.onClick.AddListener (OnColorButtonClick);
		yellowButton.onClick.AddListener (OnColorButtonClick);
		greenButton.onClick.AddListener (OnColorButtonClick);
		blueButton.onClick.AddListener (OnColorButtonClick);
		blackButton.onClick.AddListener (OnColorButtonClick);
	}

	//centralized method to handle all color button click events
	void OnColorButtonClick()
	{
		string buttonName = EventSystem.current.currentSelectedGameObject.name;
		Debug.Log (buttonName + " clicked!");

		//set trigger to close the color picker
		Animator panelAnimator = gameObject.GetComponent<Animator> ();
		panelAnimator.SetTrigger ("color set");

		//switch case to get the 'Color' from the button name
		switch (buttonName) 
		{
		case "White":
			chosenButtonColor = whiteButton.colors.normalColor;
			break;
		case "Red":
			chosenButtonColor = redButton.colors.normalColor;
			break;
		case "Yellow":
			chosenButtonColor = yellowButton.colors.normalColor;
			break;
		case "Green":
			chosenButtonColor = greenButton.colors.normalColor;
			break;
		case "Blue":
			chosenButtonColor = blueButton.colors.normalColor;
			break;
		case "Black":
			chosenButtonColor = blackButton.colors.normalColor;
			break;
		}
		//apply the color from button to the product
		sceneController.ApplyColor (chosenButtonColor);
	}
}
