PRODUCT 360 - Structure
-----------------------

Recommended Resolution: 1024x768

The app opens with the Lounge Page and transitions into Landing Page (which are actually created on the same panel)

In the Landing Page the user should choose whether he wants to Login (disabled), Signup (disabled) or enter as Guest.

In the Open/New page, the user should choose whether he wants to create a new project or open (disabled) an existing one

Then 360 viewer page is opened. By default, a cube spins in the viewport. The user has to choose between the products (currently only 2) in order to replace the cube.

There are two kinds of clicks identified through the script:

 -> Short click on the model: The Color Picker slides down from the top. The user can choose between 6 different colors to apply on the product. On choosing a color, the Color-Picker panel slides up and the color is applied on the product.

 -> Long click anywhere in the viewport: This stops the object from spinning (animation) and the object can now be turned in the direction prefered by the user with the help of mouse.

Note: The object will continue to spin (even while picking color) by default unless a long click is performed

The user can quit the application by pressing the 'Quit' button located at the bottom right on the viewer page

